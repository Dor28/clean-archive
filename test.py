import argparse
from pathlib import Path
import os
import zipfile
import logging
import shutil

logger = logging.getLogger()

parser = argparse.ArgumentParser()
parser.add_argument('dir')
args = parser.parse_args()

directory = "temp"
home = Path.cwd()
temp_path = os.path.join(home, directory)
os.mkdir(temp_path)
logger.error("Error getting home folder")
with zipfile.ZipFile(args.dir, 'r') as zip_ref:
    zip_ref.extractall(temp_path)

os.chdir(temp_path)
print()